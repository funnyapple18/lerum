package ru.funnyapple.lerumgame;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.utils.TimeUtils;

import ru.funnyapple.lerumgame.screens.GameScreen;

public class Hero extends Actor {

	private final float SPEED = 320F;
	private final float BULLET_SPAWN_PERIOD = 0.5F * 1000000000;

	private Texture texture;
	private long bulletTime;

	public Hero() {
		texture = new Texture(Gdx.files.internal("hero.png"));
		bulletTime = TimeUtils.nanoTime();

		setWidth(texture.getWidth());
		setHeight(texture.getHeight());
		setX(GameScreen.get().getRightBorder() / 2 - getWidth() / 2);
		setY(5);
	}

	@Override
	public void draw(Batch batch, float alpha) {
		if (!GameScreen.get().isPaused()) {
			listenInput();
			checkBounds();
			shoot();
		}

		batch.draw(texture, getX(), getY(), getWidth(), getHeight());
	}

	private void shoot() {
		if (TimeUtils.nanoTime() - bulletTime > BULLET_SPAWN_PERIOD) {
			bulletTime = TimeUtils.nanoTime();
			GameScreen.get().getStage()
					.addActor(new PlayerBullet(getX() + getWidth() / 2 - 6, getY() + getHeight() - 16));
		}
	}

	private void checkBounds() {
		if (getX() < 0) {
			setX(0);
		}
		if (getX() > GameScreen.get().getRightBorder() - getWidth()) {
			setX(GameScreen.get().getRightBorder() - getWidth());
		}
	}

	private void listenInput() {
		if (Gdx.input.isTouched()) {
			if (Gdx.input.getX() < Gdx.graphics.getWidth() / 2) {
				setX(getX() - (SPEED + ScoreManager.getScore() / 4) * Gdx.graphics.getDeltaTime());
			} else {
				setX(getX() + (SPEED + ScoreManager.getScore() / 4) * Gdx.graphics.getDeltaTime());
			}
		}
	}

}
