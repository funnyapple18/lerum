package ru.funnyapple.lerumgame.gui;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import ru.funnyapple.lerumgame.Main;
import ru.funnyapple.lerumgame.screens.MenuScreen;

public class MenuButton extends Actor {
	
	private TextureRegion released;
	private TextureRegion pressed;
	private TextureRegion current;
	
	private MenuScreen menu;
	
	public MenuButton(float x, float y, TextureRegion released, TextureRegion pressed) {
		this.released = released;
		this.pressed = pressed;
		current = released;
		
		setX(x);
		setY(y);
		setWidth(current.getRegionWidth());
		setHeight(current.getRegionHeight());
		
		menu = Main.get().getMenuScreen();
	}
	
	public void setPressed() {
		current = pressed;
	}
	
	public void setReleased() {
		current = released;
	}
	
	@Override
	public void draw(Batch batch, float alpha) {
		batch.draw(current, getX(), getY(), getWidth(), getHeight());
		
		if (this.equals(menu.getPressed())) {
			Main.get().setScreen(Main.get().getGameScreen(Main.get()));
		}
	}
	
}
