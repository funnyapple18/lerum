package ru.funnyapple.lerumgame;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Actor;

public class GameText extends Actor {

	private BitmapFont font = FontGenerator.gameFont();
	private String text;
	private float x;
	private float y;

	public GameText(String text, float x, float y) {
		this.text = text;
		this.x = x;
		this.y = y;
	}

	@Override
	public void draw(Batch batch, float alpha) {
		font.draw(batch, text, x, y);
	}

	public void update(String text) {
		this.text = text;
	}

}
