package ru.funnyapple.lerumgame;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Actor;
import ru.funnyapple.lerumgame.screens.GameScreen;

public class PlayerBullet extends Actor {

	private final float SPEED = 500F;

	private TextureRegion texture;
	private Rectangle rect;

	public PlayerBullet(float x, float y) {
		texture = Main.get().getTextureManager().getBulletAtlas().get("playerbullet");

		setX(x);
		setY(y);
		setWidth(texture.getRegionWidth());
		setHeight(texture.getRegionHeight());

		rect = new Rectangle(getX(), getY(), getWidth(), getHeight());
		GameManager.getPlayerBullets().add(this);
	}

	@Override
	public void draw(Batch batch, float alpha) {
		batch.draw(texture, getX(), getY(), getWidth(), getHeight());

		if (GameScreen.get().isPaused()) return;

		setY(getY() + SPEED * Gdx.graphics.getDeltaTime());

		if (getY() > GameScreen.get().getH()) {
			destroy();
		}

		rect.x = getX();
		rect.y = getY();
	}

	public Rectangle getRectangle() {
		return rect;
	}

	public void destroy() {
		GameManager.getPlayerBullets().remove(this);
		remove();
		clear();
	}

}
