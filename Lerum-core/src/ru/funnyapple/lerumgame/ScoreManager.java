package ru.funnyapple.lerumgame;

public class ScoreManager {

	private static int score = 0;

	public static void setScore(int newScore) {
		score = newScore;
	}

	public static void addScore(int add) {
		score += add;
	}

	public static void withdrawScore(int wd) {
		score -= wd;
	}

	public static int getScore() {
		return score;
	}
}
