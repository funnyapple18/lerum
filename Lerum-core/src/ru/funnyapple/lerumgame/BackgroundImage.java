package ru.funnyapple.lerumgame;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

public class BackgroundImage extends Image {
	
	private Texture t = new Texture(Gdx.files.internal("background1.png"));
	
	@Override
	public void draw(Batch b, float a) {
		b.draw(t, 0, 0);
	}
	
}
