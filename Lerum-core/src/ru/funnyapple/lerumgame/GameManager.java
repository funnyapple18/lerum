package ru.funnyapple.lerumgame;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.Timer.Task;
import ru.funnyapple.lerumgame.enemies.Enemy;
import ru.funnyapple.lerumgame.screens.GameScreen;

public class GameManager {

	private static List<PlayerBullet> playerBullets = new ArrayList<PlayerBullet>();
	private static List<Enemy> enemies = new ArrayList<Enemy>();

	private static boolean msg = false;

	public static void lose() {
		ScoreManager.setScore(0);
		GameScreen.get().dispose();
		Main.get().setScreen(Main.get().getMenuScreen());
	}

	public static List<PlayerBullet> getPlayerBullets() {
		return playerBullets;
	}

	public static List<Enemy> getEnemies() {
		return enemies;
	}

	public static boolean hasMsg() {
		return msg;
	}

	public static void setMsg(boolean m) {
		msg = m;
	}

	public static class Time {

		private static int secondsPassed = 0;

		private static Task task = new Task() {
			@Override
			public void run() {
				secondsPassed++;
			}
		};

		public static void runTimer() {
			Timer.schedule(task, 0f, 1f);
		}

		public static void stopTimer() {
			task.cancel();
		}

		public static int getPassedTime() {
			return secondsPassed;
		}

	}
}
