package ru.funnyapple.lerumgame.enemies;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.utils.TimeUtils;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.Timer.Task;

import ru.funnyapple.lerumgame.Main;
import ru.funnyapple.lerumgame.Path;
import ru.funnyapple.lerumgame.ScoreManager;
import ru.funnyapple.lerumgame.screens.GameScreen;

public class EnemySpawner extends Actor {
	
	private final long SECOND = 1000000000L;
	private final float SECTOR_PERIOD = 0.6f;
	
	private float periodSimple = 2 * SECOND;
	private float periodSnake = 60 * SECOND;
	private int snakeLenght = 5;
	
	private long deltaSimple = TimeUtils.nanoTime();
	private long deltaSnake = TimeUtils.nanoTime();
	
	@Override
	public void draw(Batch batch, float alpha) {
		if (GameScreen.get().isPaused())
			return;
		
		spawnSimpleEnemies();
		
		if (ScoreManager.getScore() > 50) {
			spawnSnake();
		}
	}
	
	private void spawnSnake() {
		if (TimeUtils.nanoTime() - deltaSnake > periodSnake) {
			deltaSnake = TimeUtils.nanoTime();
			
			Timer.schedule(new Task() {
				private int time = 0;
				
				@Override
				public void run() {
					time++;
					if (time >= snakeLenght) {
						this.cancel();
					}
					
					SnakeSector s = new SnakeSector(-50, 300);
					GameScreen.get().getStage().addActor(s);
					s.setZIndex(1);
					s.setPath(getRandomSnakePath());
					
				}
			}, 0f, SECTOR_PERIOD);
		}
	}
	
	private Path getRandomSnakePath() {
		Vector2[] waypoints = {
				new Vector2(50, 300),
				new Vector2(100, 300),
				new Vector2(200,400),
				new Vector2(325, 300),
				new Vector2(400, 400),
				new Vector2(550, 300),
				new Vector2(650, 400),
				new Vector2(750, 400),
		};
		
		Path path = new Path();
		path.addWaypoint(waypoints);
		return path;
	}

	private void spawnSimpleEnemies() {
		if (TimeUtils.nanoTime() - deltaSimple > periodSimple) {
			deltaSimple = TimeUtils.nanoTime();

			float x = MathUtils.random(GameScreen.get().getRightBorder()
					- Main.get().getTextureManager().getEnemyAtlas().get("enemy").getRegionWidth());
			float y = GameScreen.get().getH();

			Enemy enemy = new SimpleEnemy(x, y);
			GameScreen.get().getStage().addActor(enemy);

			enemy.setZIndex(1);
			periodSimple = MathUtils.random(0.2F, 1.5F) * SECOND;
		}
	}
	
}
