package ru.funnyapple.lerumgame.enemies;

import java.util.Iterator;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Actor;

import ru.funnyapple.lerumgame.GameManager;
import ru.funnyapple.lerumgame.PlayerBullet;
import ru.funnyapple.lerumgame.ScoreManager;
import ru.funnyapple.lerumgame.gui.Message;
import ru.funnyapple.lerumgame.screens.GameScreen;

public class Enemy extends Actor {

	private float speed;
	private int price;
	private int health;
	protected TextureRegion texture;
	private Rectangle rect;
	private Iterator<PlayerBullet> pbi;

	protected Enemy(TextureRegion texture, float x, float y) {
		this.texture = texture;
		setX(x);
		setY(y);
		setWidth(texture.getRegionWidth());
		setHeight(texture.getRegionHeight());
		setZIndex(1);
		setHealth(1);
		setPrice(1);
		rect = new Rectangle(getX(), getY(), getWidth(), getHeight());
		GameManager.getEnemies().add(this);
	}
	
	public void listenBehavior() {
		if (getY() < -getWidth()) {
			new Message("Game Over. Score: " + ScoreManager.getScore());
			die();
			return;
		}
	}
	
	@Override
	public void draw(Batch batch, float alpha) {
		if (!GameScreen.get().isPaused()) setY(getY() - speed * Gdx.graphics.getDeltaTime());

		listenBulletCollision();
		listenBehavior();

		//batch.draw(texture, getX(), getY(), getWidth(), getHeight());
		batch.draw(texture, getX(), getY(), getOriginX(), getOriginY(), getWidth(), getHeight(), 1, 1, getRotation());
		
		rect.x = getX();
		rect.y = getY();
	}

	private void listenBulletCollision() {
		pbi = GameManager.getPlayerBullets().iterator();

		while (pbi.hasNext()) {
			PlayerBullet bullet = pbi.next();
			
			if (rect.overlaps(bullet.getRectangle())) {
				pbi.remove();
				bullet.destroy();
				health--;
				
				if (health < 1) {
					die();
					ScoreManager.addScore(price);
				}
			}
		}
	}

	public void die() {
		GameManager.getEnemies().remove(this);
		remove();
		clear();
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public void setSpeed(float speed) {
		this.speed = speed;
	}

	public void setHealth(int health) {
		this.health = health;
	}

	public int getHealth() {
		return health;
	}

}
