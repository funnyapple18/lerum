package ru.funnyapple.lerumgame.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.StretchViewport;

import ru.funnyapple.lerumgame.BackgroundImage;
import ru.funnyapple.lerumgame.GameManager;
import ru.funnyapple.lerumgame.Hero;
import ru.funnyapple.lerumgame.Main;
import ru.funnyapple.lerumgame.ScoreManager;
import ru.funnyapple.lerumgame.GameText;
import ru.funnyapple.lerumgame.enemies.EnemySpawner;
import ru.funnyapple.lerumgame.gui.Message;
import ru.funnyapple.lerumgame.gui.RightBlock;

public class GameScreen implements Screen {

	private static GameScreen instance;
	private Stage stage;
	private Hero hero;
	private EnemySpawner enemySpawner;
	private GameText scoreText;
	private BackgroundImage background;
	private OrthographicCamera cam;
	private StretchViewport viewport;
	private RightBlock rightBlock;
	private boolean pause;

	private GameText fps;

	public GameScreen(Main gameClass) {
		instance = this;
	}

	public static GameScreen get() {
		return instance;
	}

	@Override
	public void show() {
		pause = false;
		cam = new OrthographicCamera();
		cam.setToOrtho(false);
		viewport = new StretchViewport(Main.W, Main.H, cam);
		stage = new Stage(viewport);
		hero = new Hero();
		background = new BackgroundImage();
		enemySpawner = new EnemySpawner();
		rightBlock = new RightBlock();
		scoreText = new GameText("score", getRightBlock().getX() + 12, getH() - 16);
		fps = new GameText("fps", getRightBlock().getX() + 12, getH() - 32);

		stage.getViewport().setCamera(cam);
		stage.addActor(background);
		stage.addActor(hero);
		stage.addActor(enemySpawner);
		stage.addActor(rightBlock);
		stage.addActor(scoreText);
		stage.addActor(fps);
		
		background.setZIndex(0);
		hero.setZIndex(2);
		rightBlock.setZIndex(3);
		scoreText.setZIndex(4);

		Gdx.input.setInputProcessor(null);
		Gdx.input.setCatchBackKey(true);

		GameManager.Time.runTimer();
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		scoreText.update("Score: " + ScoreManager.getScore());
		fps.update("FPS: " + Math.round(1 / delta) + "|" + GameManager.Time.getPassedTime());
		/*
		 * if (Gdx.input.isCatchBackKey()) { if (isPaused()) {
		 * Main.get().setScreen(Main.get().getMenuScreen()); } else {
		 * setPaused(true); } }
		 */

		stage.act(delta);
		stage.draw();
	}

	public Stage getStage() {
		return stage;
	}

	public float getW() {
		return Main.W;
	}

	public float getH() {
		return Main.H;
	}

	public float getRightBorder() {
		return Main.W - 100;
	}

	public RightBlock getRightBlock() {
		return rightBlock;
	}

	public boolean isPaused() {
		return pause;
	}

	public void setPaused(boolean pause) {
		this.pause = pause;
	}

	@Override
	public void dispose() {
		GameManager.Time.stopTimer();
		Gdx.input.setCatchBackKey(false);
		stage.dispose();
		stage.clear();
	}

	@Override
	public void resize(int width, int height) {

	}

	@Override
	public void pause() {
		new Message("Game Paused. Score: " + ScoreManager.getScore());
	}

	@Override
	public void resume() {

	}

	@Override
	public void hide() {

	}

}
